INCLUDE 'emu8086.inc' 
#MAKE_COM#
ORG 100H              
JMP MULAI             
PSN1    DB "MENGHITUNG LUAS JAJARGENJANG",13,10,'$'
PSN2    DB "MASUKKAN NILAI ALAS JAJARGENJANG: $"
PSN3    DB "MASUKKAN NILAI TINGGI JAJARGENJANG: $"
PSN4    DB "LUAS JAJARGENJANG TERSEBUT ADALAH: $"
PSN5    DB 13,10,13,10,"TEKAN APA SAJA UNTUK KELUAR $"
ALS     DW ?
TGI     DW ?
ARE     DW ?

MULAI:
LEA DX,PSN1           
MOV AH,9              
INT 21H               

LEA DX,PSN2
MOV AH,9              
INT 21H

CALL SCAN_NUM         

MOV ALS,CX            

PUTC 13               
PUTC 10

LEA DX,PSN3
MOV AH,9              
INT 21H

CALL SCAN_NUM         

MOV TGI,CX            

MOV AX,TGI            
MOV BX,ALS            
IMUL BX
PUSH AX

PUTC 13               
PUTC 10

LEA DX,PSN4
MOV AH,9              
INT 21H

POP AX
MOV ARE,AX            

CALL PRINT_NUM        
                      
PUTC 13               
PUTC 10
LEA DX,PSN5
MOV AH,9              
INT 21H

MOV AH,0              
INT 16H

RET                   

DEFINE_SCAN_NUM       
DEFINE_PRINT_NUM
DEFINE_PRINT_NUM_UNS
END
